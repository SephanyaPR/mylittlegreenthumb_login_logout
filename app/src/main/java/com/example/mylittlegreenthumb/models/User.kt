package com.example.mylittlegreenthumb.models

data class User(val user_id:Int, val email_id: String?, val user_type:Int)