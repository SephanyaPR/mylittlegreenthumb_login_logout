package com.example.mylittlegreenthumb.api

import com.example.mylittlegreenthumb.models.LoginResponse
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface Api {

    @FormUrlEncoded
    @POST("login")
    fun loginUser(
        @Field("email_id") email:String,
        @Field("password")password:String,
        @Field("user_type")usertype:Int
    ):Call<LoginResponse>
}